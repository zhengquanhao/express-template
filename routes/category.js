var express = require('express');
var router = express.Router();

let category = require("../controllers/categoryController")

/* GET category page. */
router.get('/getCategory',category.getCategory);

module.exports = router;
