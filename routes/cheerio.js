var dbConfig = require("../util/dbconfig");

var express = require('express');
var router = express.Router();

let axios = require('axios');
const cheerio = require('cheerio');

function insertPruduct(productId,mainImgSrc,iconImgSrc,promotionDiscount,price,wpPrice,markPrice,discount,productName,brandId) {
  let sql = "insert into product (brand_id,productId,mainImgSrc,iconImgSrc,promotionDiscount,price,wpPrice,markPrice,discount,productName,brandId) values (?,?,?,?,?,?,?,?,?,?,?)";
    let sqlArr = ["100631568",productId,mainImgSrc,iconImgSrc,promotionDiscount,price,wpPrice,markPrice,discount,productName,brandId];
    let callback = (err,data) => {
        if (err) {
            console.log("连接出错");
        return;
        } else {
          console.log("连接成功");
        }
    }
  dbConfig.sqlConnect(sql,sqlArr,callback);
}

router.get('/', function(req, res, next) {
  res.send('test');
  axios.get("https://m.vip.com/brand-100624618-0-0-0-1-0-1-20.html").then(res => {
    let $ = cheerio.load(res.data);
    $("#J-list-view .u-product").each( (i,item) => {
      let brand_id = "100631568"
      let mainImgSrc = $(item).find(".u-product-pic > img").attr('src')
      let iconImgSrc = $(item).find(".u-product-pic .u-icon img").attr('src')
      // 折后价text
      let promotionDiscount = $(item).find(".product-price-wrap .vip-price-msg span").text()
      // 
      let price = $(item).find(".product-price-wrap .vip-promotion-price .promotion-price-wrapper").text()
      let wpPrice = $(item).find(".grally-price-wrapper .grally-price .vip-price-wrapper").text()
      // 原价
      let markPrice = $(item).find(".grally-price-wrapper .mark-price").text()
      // 折扣
      let discount = $(item).find(".grally-price-wrapper .discount").text()
      let productName = $(item).find(".product-name").text().trim()
      let productMap = $(item).find(".u-product > a").attr("data-rawurl")
      let brandId = productMap.split("-")[1]
      let productId = productMap.split("-")[2].slice(0,19)
      insertPruduct(productId,mainImgSrc,iconImgSrc,promotionDiscount,price,wpPrice,markPrice,discount,productName,brandId)
    })
  })
});

module.exports = router;
