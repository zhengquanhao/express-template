var dbConfig = require("../util/dbconfig");

// 获取分类
getCategory = (req,res)=> {
    let sql = "select * from category";
    let sqlArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log("连接出错");
        return;
        } else {
            // 返回数据
            res.send({
                "list":data
            })
        }
    }
    dbConfig.sqlConnect(sql,sqlArr,callback);
}

// 获取指定分类的列表
getCategoryById = (req,res) => {
    let {id} = req.query;
    let sql = `select * from category where id=?`
    let sqlArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log("连接出错");
        return;
        } else {
            // 返回数据
            res.send({
                "list":data
            })
        }
    }
    dbConfig.sqlConnect(sql,sqlArr,callback);
}

module.exports = {
    getCategory
}